#!/bin/bash

if [ -e /etc/nginx/sites-available/flight.conf ]
then
    sudo rm -rf /etc/nginx/sites-available/flight.conf;
fi

if [ -L /etc/nginx/sites-enabled/flight.conf ]
then
    sudo rm -rf /etc/nginx/sites-enabled/flight.conf;
fi

sudo cp /var/www/html/flightapi/flight.conf /etc/nginx/sites-available/;
sudo ln -s /etc/nginx/sites-available/flight.conf /etc/nginx/sites-enabled/;
sudo service nginx restart;

php composer.phar install
