<?php

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 1/31/18
 * Time: 3:13 PM
 */

namespace console\controllers;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use common\components\Chat;

class CmdController extends \yii\console\Controller
{
    public function actionInit()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat()
                )
            ),
            8000
        );

        $server->run();
    }
}