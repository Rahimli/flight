<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

$rootPath = dirname(dirname(__DIR__)).'/frontend/web';

Yii::setAlias('@ckeditorRoot', $rootPath . '/uploads/ckeditor');
Yii::setAlias('@ckeditor', '/uploads/ckeditor');
//profile
Yii::setAlias('@profileRoot', $rootPath . '/uploads/profile');
Yii::setAlias('@profile', '/uploads/profile');
//document
Yii::setAlias('@documentRoot', $rootPath . '/uploads/document');
Yii::setAlias('@document', '/uploads/document');
//static
Yii::setAlias('@staticRoot', $rootPath . '/uploads/static');
Yii::setAlias('@static', '/uploads/static');
//news
Yii::setAlias('@newsRoot', $rootPath . '/uploads/news');
Yii::setAlias('@news', '/uploads/news');