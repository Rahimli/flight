<?php
/**
 * @var \common\models\User $model
*/
$profileLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-registration', 'key' => $model->getAuthKey()]);
?>
<tr>
    <td style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
        <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color: #646465;text-align: left;">
            <?=Yii::t('main','Activate your account')?>
        </div>
        <br>

        <div class="body-text"
             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">

            <?=Yii::t('main','Hello')?> <?=$model->username;?>,
            <p style="margin-bottom: 0;">
            <?=Yii::t('main','Please activate your account on Shofero by clicking on button below:')?>
            <a style="width: 180px;text-align: center;padding: 14px 0;font-size: 18px;display: block;margin:auto;background: #00A6E2;color: #fff;text-decoration: none;border-radius: 2px;margin-top: 40px;" href="<?=$profileLink?>"><?=Yii::t('main','Activate')?></a>

            </p>
        </div>
    </td>
</tr>