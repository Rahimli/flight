<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/15/17
 * Time: 2:34 PM
 */
?>

<tr>
    <td style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
        <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color: #646465;text-align: left;">
            <?=Yii::t('main','Shoferoya xoş gəlminiz!')?>
        </div>
        <br>

        <div class="body-text"
             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">

            <?=Yii::t('main','Salam')?> <?=$model->username;?>,
            <p style="margin-bottom: 0;">
                <?=Yii::t('main','Sizi saytımızda görməkdən məmnunuq!')?>
            </p>
        </div>
    </td>
</tr>
