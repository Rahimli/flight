<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 1/26/18
 * Time: 7:39 PM
 */

namespace common\models;


use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @property UploadedFile $conviction_certificate
     * @property UploadedFile $driving_license
     */
    public $conviction_certificate;
    public $driving_license;
    public $voen_front;
    public $voen_back;

    public function rules()
    {
        return [
            [['driving_license', 'conviction_certificate'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, png'],
            [['voen_front', 'voen_back'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, png'],
        ];
    }

    public function upload($alias)
    {
        if ($this->validate()) {
            $fileName = substr(sha1(time() . rand(1000, 9999)), 0, 16) . '.' . $this->file->extension;
            $path = \Yii::getAlias('@'.$alias.'Root/');

            $fullPath = $path . $fileName;
            if (!is_dir($path)) {
                FileHelper::createDirectory($path);
            }
            $this->driving_license->saveAs($fullPath);

            $imageName = substr(sha1(time() . rand(1000, 9999)), 0, 16) . '.' . $this->file->extension;
            $this->file->saveAs($fullPath);


            return true;
        } else {
            return false;
        }
    }

    public function getFileName()
    {

    }
}