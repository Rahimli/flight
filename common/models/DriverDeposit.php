<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%driver_deposit}}".
 *
 * @property int $id
 * @property int $driver_id
 * @property int $deposit_count
 * @property double $amount
 * @property int $created_at
 *
 * @property Driver $driver
 * @property DriverRide $driverRide
 */
class DriverDeposit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver_deposit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'deposit_count', 'amount', 'created_at'], 'required'],
            [['driver_id', 'deposit_count', 'created_at'], 'integer'],
            [['amount'], 'number'],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'driver_id' => Yii::t('main', 'Driver ID'),
            'deposit_count' => Yii::t('main', 'Deposit Count'),
            'amount' => Yii::t('main', 'Amount'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverRide()
    {
        return $this->hasOne(DriverRide::className(), ['driver_id' => 'driver_id']);
    }
}
