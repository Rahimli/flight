<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sd_car_make".
 *
 * @property int $id
 * @property int $enabled
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CarModel[] $carModels
 * @property UserVehicle[] $userVehicles
 */
class CarMake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_make}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['enabled', 'created_at', 'updated_at'], 'integer'],
            [['name', 'enabled'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'language',
                'defaultLanguage' => $_SESSION['language'],
                'langForeignKey' => 'make_id',
                'tableName' => "{{%car_make_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'enabled' => Yii::t('main', 'Enabled'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['make_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVehicles()
    {
        return $this->hasMany(UserVehicle::className(), ['make_id' => 'id']);
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }
}
