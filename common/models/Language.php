<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $code
 * @property string $language
 * @property integer $enabled
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'language'], 'required'],
            [['enabled'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['language'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'code' => Yii::t('main', 'Code'),
            'language' => Yii::t('main', 'Language'),
            'enabled' => Yii::t('main', 'Enabled'),
        ];
    }
}
