<?php

namespace common\models;

use frontend\modules\product\models\Product;
use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "color".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $value
 * @property integer $enabled
 *
 */
class Color extends \yii\db\ActiveRecord
{
    const ENABLED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%color}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
            ['name', 'string', 'max' => 50],
            [['created_at', 'updated_at', 'enabled'], 'integer'],
            [['value'], 'string', 'max' => 50],
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'language',
                'defaultLanguage' => $_SESSION['language'],
                'langForeignKey' => 'color_id',
                'tableName' => "{{%color_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'value' => Yii::t('main', 'Value'),
            'enabled' => Yii::t('main', 'Enabled'),
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }
}
