<?php
namespace common\models;


use Yii;

/**
 * Driver model
 *
 * @property DriverDeposit $driverDeposit
*/

class Driver extends User
{
    const STATUS_FREE = 1;
    const STATUS_BUSY = 2;

    public function createDriverRide():void
    {
        if ($this->type_id == self::TYPE_DRIVER) {
            $driverRide = DriverRide::findOne(['driver_id' => $this->id]);
            if (empty($driverRide)) {
                $driverRide = new DriverRide();
                $driverRide->driver_id = $this->id;
                $driverRide->save();
            }
        }
    }

    public function createDriverDeposit():void
    {
        if ($this->type_id == self::TYPE_DRIVER) {
            $deposit = new DriverDeposit();
            $deposit->driver_id = $this->id;
            $deposit->deposit_count = Yii::$app->params['defaultDeposit'];
            $deposit->amount = Yii::$app->params['defaultDeposit'] * Yii::$app->params['depositRate'];
            $deposit->created_at = time();
            $deposit->save();

            $this->driverDeposit->deposit_count += $deposit->deposit_count;
            $this->driverDeposit->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverDeposit()
    {
        return $this->hasOne(DriverDeposit::className(), ['driver_id' => 'id']);
    }
}
