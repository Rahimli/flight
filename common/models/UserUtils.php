<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 1/21/18
 * Time: 2:51 PM
 */

namespace common\models;


use Yii;

class UserUtils
{
    const TYPE_FB = 'facebook';
    const TYPE_GP = 'google';

    /**
     * @param string $email
     * @param string $first_name
     * @param string $last_name
     * @param string $social
     * @return array user logged in data or validation error
     */
    public static function socialLogin($email, $first_name, $last_name, $social)
    {
        if (Yii::$app->user->isGuest && $email) {
            // signup
            if ($user=User::findByEmail($email)) {
                Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                $user->login_count++;
                $user->save();

                return [
                    'userData' => $user->getLoggedInData(),
                    'success' => 1,
                    'completed_signup' => (int)!empty($user->phone)
                ];
            } else {
                $user = new User();

                //set scenario social
                $user->scenario = User::SCENARIO_SOCIAL;
                $user->email = $email;
                $user->login_count = 1;
                $user->status = User::STATUS_ACTIVE;
                $user->ip_address = Yii::$app->request->getUserIP();
                $user->first_name = $first_name;
                $user->last_name = $last_name;
                $user->social = $social;

                $user->username = $user->first_name . ' ' . $user->last_name;
                $user->generateAuthKey();

                if ($user->save()) {
                    Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                    return [
                        'userData' => $user->getLoggedInData(),
                        'success' => 1,
                        'completed_signup' => 0
                    ];
                } else {
                    return [
                        'success' => 0,
                        'messages' => array_values($user->getFirstErrors()),
                        'completed_signup' => 0
                    ];
                }
            }

        }
    }

    public static function userTypes()
    {
        return [
            User::TYPE_RIDER => Yii::t('main', 'Rider'),
            User::TYPE_DRIVER => Yii::t('main', 'Driver'),
        ];
    }

    public static function getTypeById(int $id)
    {
        if (isset(self::userTypes()[$id])) {
            return self::userTypes()[$id];
        }
        return null;
    }

    public static function userStatutes()
    {
        return [
            User::STATUS_DELETED => Yii::t('main', 'Inactive'),
            User::STATUS_ACTIVE => Yii::t('main', 'Active'),
        ];
    }

    public static function getStatusById(int $id)
    {
        if (isset(self::userStatutes()[$id])) {
            return self::userStatutes()[$id];
        }
        return null;
    }

    public static function signupMethods()
    {
        return [
            User::SIGNUP_MANUAL => Yii::t('main', 'Manual'),
            User::SIGNUP_FB => Yii::t('main', 'Facebook'),
            User::SIGNUP_GP => Yii::t('main', 'Google plus'),
        ];
    }

    public static function getsignupMethodById(int $id)
    {
        if (isset(self::signupMethods()[$id])) {
            return self::signupMethods()[$id];
        }
        return null;
    }

    public static function roles()
    {
        return [
            User::ROLE_MODERATOR => Yii::t('main', 'Moderator'),
            User::ROLE_ADMIN => Yii::t('main', 'Admin'),
        ];
    }

    public static function getRoleByName($name)
    {
        if (isset(self::roles()[$name])) {
            return self::roles()[$name];
        }
        return null;
    }
}