<?php

namespace common\models;

use common\components\order\DistanceCalculator;
use common\components\order\OrderUtils;
use Yii;

/**
 * This is the model class for table "sd_order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $driver_id
 * @property int $vehicle_id
 * @property string $from_location
 * @property double $from_latitude
 * @property double $from_longitude
 * @property string $to_location
 * @property double $to_latitude
 * @property double $to_longitude
 * @property double $distance
 * @property double $price
 * @property int $scheduled_at
 * @property int $started_at
 * @property int $ended_at
 * @property int $created_at
 * @property int $status_id
 *
 * @property User $user
 * @property User $driver
 * @property UserVehicle $vehicle
 * @property Review $userReview
 * @property Review $driverReview
 * @property OrderStopPoint[] $orderStopPoints
 * @property DriverMessage[] $driverMessages
 * @property UserPayment[] $userPayments
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_CANCELLED_BY_USER = 2;
    const STATUS_CANCELLED_BY_DRIVER = 3;
    const STATUS_DRIVER_ARRIVED = 4;
    const STATUS_ONGOING = 5;
    const STATUS_COMPLETED = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'driver_id', 'vehicle_id', 'from_location', 'from_latitude', 'from_longitude', 'status_id'], 'required'],
            [['user_id', 'driver_id', 'vehicle_id', 'scheduled_at', 'started_at', 'ended_at', 'created_at', 'status_id'], 'integer'],
            [['from_latitude', 'from_longitude', 'to_latitude', 'to_longitude', 'distance', 'price'], 'number'],
            [['from_location', 'to_location'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['driver_id' => 'id']],
            [['vehicle_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserVehicle::className(), 'targetAttribute' => ['vehicle_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('main', 'User'),
            'driver_id' => Yii::t('main', 'Driver'),
            'vehicle_id' => Yii::t('main', 'Vehicle'),
            'from_location' => Yii::t('main', 'From'),
            'from_latitude' => Yii::t('main', 'From Latitude'),
            'from_longitude' => Yii::t('main', 'From Longitude'),
            'to_location' => Yii::t('main', 'To'),
            'to_latitude' => Yii::t('main', 'To Latitude'),
            'to_longitude' => Yii::t('main', 'To Longitude'),
            'distance' => Yii::t('main', 'Distance (m)'),
            'price' => Yii::t('main', 'Price (AZN)'),
            'scheduled_at' => Yii::t('main', 'Scheduled At'),
            'started_at' => Yii::t('main', 'Started At'),
            'ended_at' => Yii::t('main', 'Ended At'),
            'created_at' => Yii::t('main', 'Created At'),
            'status_id' => Yii::t('main', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(UserVehicle::className(), ['id' => 'vehicle_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverReview()
    {
        return $this->hasOne(Review::className(), ['order_id' => 'id'])->andWhere(['type_id' => Review::TYPE_DRIVER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserReview()
    {
        return $this->hasOne(Review::className(), ['order_id' => 'id'])->andWhere(['type_id' => Review::TYPE_USER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStopPoints()
    {
        return $this->hasMany(OrderStopPoint::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverMessages()
    {
        return $this->hasMany(DriverMessage::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPayments()
    {
        return $this->hasMany(UserPayment::className(), ['order_id' => 'id']);
    }

    public function getStatusName()
    {
        return OrderUtils::getStatuses()[$this->status_id];
    }

    public function setOrderDistance()
    {
        $orderLocs = OrderUtils::getOriginAndDestinationFromOrder($this);
        $distance = DistanceCalculator::getDistanceWithMeter($orderLocs['origin'], $orderLocs['destination']);
        $this->distance = $distance;
    }
}
