<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%driver_ride}}".
 *
 * @property int $id
 * @property int $driver_id
 * @property int $accept_count
 * @property int $cancel_count
 * @property int $completed_count
 * @property int $online_seconds
 * @property int $deposit_count
 * @property double $amount
 *
 * @property User $driver
 */
class DriverRide extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver_ride}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id'], 'required'],
            [['driver_id', 'accept_count', 'cancel_count', 'deposit_count', 'completed_count', 'online_seconds'], 'integer'],
            // default values
            [['accept_count', 'cancel_count', 'completed_count', 'online_seconds'], 'default', 'value' => 0],
            [['amount'], 'number'],

            ['driver_id', 'unique', 'targetClass' => '\common\models\DriverRide',
                'message' => Yii::t('main','duplicate driver.')
            ],

            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Driver',
            'accept_count' => 'Accept Count',
            'cancel_count' => 'Cancel Count',
            'completed_count' => 'Completed Count',
            'online_seconds' => 'Online Seconds',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverDeposits()
    {
        return $this->hasMany(DriverDeposit::className(), ['driver_id' => 'driver_id']);
    }

    public function getOnlineHours()
    {
        $hours = floor($this->online_seconds / 3600);
        $minutes = floor(($this->online_seconds - $hours*3600) / 60);

        return $hours . ' saat' . ' ' . $minutes . ' dəqiqə';
    }

    public function getAcceptedRate()
    {
        $percent = $this->accept_count > 0 ? (($this->completed_count / $this->accept_count) * 100) : 0;
        return number_format($percent, 2, '.', ' ');
    }
}
