<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sd_user_vehicle".
 *
 * @property integer $id
 * @property integer $make_id
 * @property integer $model_id
 * @property integer $user_id
 * @property integer $year
 * @property string $car_number
 * @property integer $color_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property CarMake $make
 * @property CarModel $model
 */
class UserVehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_vehicle}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make_id', 'model_id', 'user_id'], 'required'],
            [['make_id', 'model_id', 'user_id', 'color_id', 'created_at', 'updated_at'], 'integer'],
            [['year'], 'integer'],
            [['car_number'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['make_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarMake::className(), 'targetAttribute' => ['make_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'make_id' => Yii::t('main', 'Make ID'),
            'model_id' => Yii::t('main', 'Model ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'year' => Yii::t('main', 'Year'),
            'car_number' => Yii::t('main', 'Car Number'),
            'color_id' => Yii::t('main', 'Color ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMake::className(), ['id' => 'make_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModel::className(), ['id' => 'model_id']);
    }
}
