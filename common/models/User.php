<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $image
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $company
 * @property string $position
 * @property string $auth_key
 * @property string $ip_address
 * @property integer $country_id
 * @property integer $type_id
 * @property integer $language_id
 * @property integer $login_count
 * @property integer $social
 * @property integer $status
 * @property string $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property UserVehicle[] $vehicles rider cars
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const TYPE_RIDER = 1;
    const TYPE_DRIVER = 2;

    const SCENARIO_MANUAL = 'manual';
    const SCENARIO_SOCIAL = 'social';

    const SIGNUP_MANUAL = 1;
    const SIGNUP_FB = 2;
    const SIGNUP_GP = 3;

    const ROLE_ADMIN = 'admin';
    const ROLE_MODERATOR = 'moderator';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_count', 'type_id', 'social', 'country_id', 'language_id'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
            [['ip_address'], 'ip'],

            [['phone'], 'string'],
            [['address'], 'string', 'max' => 255],

            [['first_name', 'last_name', 'email'], 'trim'],
            [['first_name', 'last_name', 'email', 'phone'], 'required'],

            [['username'], 'string','max' => 64],

            [['role'], 'string','max' => 32],

            [['first_name', 'last_name', 'phone', 'image'], 'string', 'min' => 3, 'max' => 32],

            [['email'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_DELETED],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('main','This email address has already been taken.')
            ],

            [
                ['first_name', 'last_name', 'email', 'phone'],
                'filter',
                'filter' => function ($value) {
                    return filter_var($value, FILTER_SANITIZE_STRING);
                }
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_MANUAL] = ['first_name', 'last_name', 'email', 'username', 'login_count', 'social', 'language_id', 'status', 'country_id', 'created_at', 'updated_at', 'ip_address', 'phone', 'type_id', 'address', 'image'];
        $scenarios[self::SCENARIO_SOCIAL] = ['first_name', 'last_name', 'email', 'username', 'login_count', 'social', 'language_id', 'status', 'country_id', 'created_at', 'updated_at', 'ip_address'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('main', 'First name'),
            'last_name' => Yii::t('main', 'Last name'),
            'language_id' => Yii::t('main', 'Language'),
            'country_id' => Yii::t('main', 'Country'),
            'address' => Yii::t('main', 'Address'),
            'type_id' => Yii::t('main', 'Type'),
            'social' => Yii::t('main', 'Signup method'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function setIpAddress()
    {
        $this->ip_address = Yii::$app->request->getUserIP();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicles()
    {
        return $this->hasMany(UserVehicle::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLoggedInData()
    {
        $userArr = [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'status' => $this->status,
            'type_id' => $this->type_id,
            'token' => $this->auth_key,
            'completed' => ($this->type_id == self::TYPE_DRIVER) ? 1 : ((int)(count($this->vehicles) > 0))
        ];

        return $userArr;
    }

    public function getProfileImg()
    {
        return $this->image ? Yii::getAlias('@profile/') . $this->image : '/images/profilephoto.png';
    }

    public function assignRole($oldRole)
    {
        if (Yii::$app->user->can('admin') && UserUtils::getRoleByName($this->role)) {
            $auth = \Yii::$app->authManager;
            $authorRole = $auth->getRole($this->role);
            $authorOldRole = $auth->getRole($oldRole);

            //remove old role
            if ($oldRole) {
                Yii::$app->authManager->revoke( $authorOldRole, $this->id );
            }

            //assign new role
            $auth->assign($authorRole, $this->id);
        }
    }
}
