<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sd_order_stop_point".
 *
 * @property int $id
 * @property int $order_id
 * @property string $location
 * @property double $latitude
 * @property double $longitude
 *
 * @property Order $order
 */
class OrderStopPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_stop_point}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'location', 'latitude', 'longitude'], 'required'],
            [['order_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['location'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'order_id' => Yii::t('main', 'Order ID'),
            'location' => Yii::t('main', 'Location'),
            'latitude' => Yii::t('main', 'Latitude'),
            'longitude' => Yii::t('main', 'Longitude'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
