<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%driver_message}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $driver_id
 * @property string $message
 * @property int $is_read
 * @property int $order_id
 * @property int $created_at
 */
class DriverMessage extends \yii\db\ActiveRecord
{
    const NOT_READ = 0;
    const HAS_READ = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'driver_id', 'order_id', 'message'], 'required'],
            [['user_id', 'driver_id', 'is_read', 'created_at'], 'integer'],
            [['message'], 'string'],
            [['message'], 'filter', 'filter' => function($val) {
                return filter_var($val, FILTER_SANITIZE_STRING);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'driver_id' => Yii::t('main', 'Driver ID'),
            'message' => Yii::t('main', 'Message'),
            'is_read' => Yii::t('main', 'Is Read'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
