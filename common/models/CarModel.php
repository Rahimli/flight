<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sd_car_model".
 *
 * @property int $id
 * @property int $enabled
 * @property int $make_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CarMake $make
 * @property UserVehicle[] $userVehicles
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_model}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enabled', 'make_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'make_id'], 'required'],
            [['name'], 'string'],
            [['make_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarMake::className(), 'targetAttribute' => ['make_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'language',
                'defaultLanguage' => $_SESSION['language'],
                'langForeignKey' => 'model_id',
                'tableName' => "{{%car_model_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'enabled' => Yii::t('main', 'Enabled'),
            'make_id' => Yii::t('main', 'Make ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMake::className(), ['id' => 'make_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVehicles()
    {
        return $this->hasMany(UserVehicle::className(), ['model_id' => 'id']);
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }
}
