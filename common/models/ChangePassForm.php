<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ChangePassForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['oldPassword', 'newPassword', 'confirmPassword'], 'required'],

            ['confirmPassword', 'compare', 'compareAttribute' => 'newPassword'],
            [['newPassword', 'confirmPassword'], 'string', 'min' => 8, 'max' => 32],
            [['oldPassword'], 'validatePassword'],
            ['confirmPassword', 'compare', 'compareAttribute'=>'newPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Password',
            'newPassword' => 'New password',
            'confirmPassword' => 'Confirm password',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldPassword)) {
                $this->addError($attribute, 'Incorrect password');
            }
        }
    }

    public function setPassword()
    {
        $user = $this->getUser();
        $user->setPassword($this->confirmPassword);
        $user->generateAccessToken();
        $user->save();
    }

    /**
     * Finds user by [[email]]
     *
     * @return Authentication|null
     */
    public function getUser()
    {
        if (!$this->_user ) {
            $this->_user = Authentication::findByEmail(Yii::$app->user->identity->Email);
        }

        return $this->_user;
    }
}
