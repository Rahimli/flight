<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sd_driver_document".
 *
 * @property int $id
 * @property int $driver_id
 * @property string $id_card
 * @property string $driving_license
 * @property string $conviction_certificate
 *
 * @property User $driver
 */
class DriverDocument extends \yii\db\ActiveRecord
{
    const SCENARIO_FIRST = 'first';
    const SCENARIO_FULL = 'full';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'driving_license', 'conviction_certificate', 'id_card'], 'required'],
            [['driver_id'], 'integer'],
            ['driver_id', 'unique', 'targetClass' => '\common\models\DriverDocument',
                'message' => Yii::t('main','duplicate driver.')
            ],
            [['driving_license', 'conviction_certificate', 'id_card'], 'string', 'max' => 32],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'driver_id' => Yii::t('main', 'Driver'),
            'driving_license' => Yii::t('main', 'Driving license'),
            'conviction_certificate' => Yii::t('main', 'Conviction certificate'),
            'id_card' => Yii::t('main', 'Identity card'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_FIRST] = ['driver_id', 'driving_license'];
        $scenarios[self::SCENARIO_FULL] = ['driver_id', 'driving_license', 'card_id', 'conviction_certificate'];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }
}
