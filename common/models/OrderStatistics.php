<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 2/23/18
 * Time: 5:50 PM
 */

namespace common\models;


use common\components\helpers\DateUtils;

class OrderStatistics
{
    public static function getCompletedOrders($driverId) {
        $orders = Order::find()
            ->select('started_at, distance, price, vehicle_id')
            ->where(['driver_id' => $driverId, 'status_id' => Order::STATUS_COMPLETED])
            ->with(['vehicle', 'vehicle.make','vehicle.make.translation', 'vehicle.model', 'vehicle.model.translation'])
            ->asArray()->all();

        $orders = array_map(function($el) {
            $el['automobile'] = $el['vehicle']['make']['translation']['name'] . ' ' . $el['vehicle']['model']['translation']['name'];
            unset($el['vehicle'], $el['vehicle_id']);
            return $el;
        }, $orders);

        return $orders;
    }

    public static function getCompletedOrdersByWeek($orders)
    {
        $ordersByWeek = [];
        foreach ($orders as $order) {
            $week = date('W', $order['started_at']);
            $year = date('Y', $order['started_at']);
            $key = $year . '_' . $week; // 2018_20 => 20nd week of the year 2018
            if (empty($ordersByWeek[$key]['label'])) {
                $weekStartEnd = DateUtils::getWeekStartAndEnd($week, $year, 'd M, Y');
                $ordersByWeek[$key]['label'] = $weekStartEnd['start'] . ' - ' . $weekStartEnd['end'];
            }
            $ordersByWeek[$key]['items'][] = $order;
        }

        return $ordersByWeek;
    }

    public static function getCompletedOrdersByDay($orders)
    {
        $ordersByDay = [];
        foreach ($orders as $order) {
            $day = date('z', $order['started_at']);
            $key = date('Y', $order['started_at']) . '_' . $day; // 2018_20 => 20nd day of the year 2018
            if (empty($ordersByDay[$key]['label'])) {
                $ordersByDay[$key]['label'] = date('d M, Y', $order['started_at']);
            }
            $ordersByDay[$key]['items'][] = $order;
        }

        return $ordersByDay;
    }

    public static function getCompletedOrdersLastWeek($orders)
    {
        $oneWeek = time() - (7*24*60*60);
        return array_filter($orders, function($el) use ($oneWeek) {
            return $el['started_at'] > $oneWeek;
        });
    }

}
