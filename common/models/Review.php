<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $driver_id
 * @property int $order_id
 * @property string $type_id
 * @property int $rating
 * @property string $review
 * @property int $created_at
 *
 * @property Order $order
 */
class Review extends \yii\db\ActiveRecord
{
    const TYPE_USER = 'user';
    const TYPE_DRIVER = 'driver';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'driver_id', 'order_id', 'type_id', 'rating', 'review', 'created_at'], 'required'],
            [['user_id', 'driver_id', 'order_id', 'rating', 'created_at'], 'integer'],
            [['type_id', 'review'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'driver_id' => Yii::t('main', 'Driver ID'),
            'order_id' => Yii::t('main', 'Order ID'),
            'type_id' => Yii::t('main', 'Type ID'),
            'rating' => Yii::t('main', 'Rating'),
            'review' => Yii::t('main', 'Review'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    public function getTypeLabel()
    {
        $key = 'Driver';
        if ($this->type_id == self::TYPE_USER) {
            $key = 'Rider';
        }

        return Yii::t('main', $key);
    }
}
