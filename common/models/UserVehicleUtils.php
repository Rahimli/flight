<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 1/25/18
 * Time: 7:14 PM
 */

namespace common\models;


use Yii;

class UserVehicleUtils
{
    public static function getVehicleData(): array
    {
        $makes = CarMake::find()
            ->select('sd_car_make.id, sd_car_make_lang.name')
            ->joinWith(['translation'])
            ->where(['enabled' => 1])
            ->localized(Yii::$app->language)
            ->asArray()
            ->all();

        $makes = array_map(function($el) {
            $models = CarModel::find()
                ->select('sd_car_model.id, sd_car_model_lang.name')
                ->joinWith(['translation'])
                ->where(['enabled' => 1, 'make_id' => $el['id']])
                ->localized(Yii::$app->language)
                ->asArray()
                ->all();
            $models = array_map(function($el) {
                unset($el['translation']);
                return $el;
            }, $models);
            $el['models'] = $models;
            unset($el['translation']);

            return $el;
        }, $makes);

        $colors = Color::find()
            ->select('sd_color.id, sd_color_lang.name')
            ->joinWith(['translation'])
            ->where(['enabled' => 1])
            ->localized(Yii::$app->language)
            ->asArray()
            ->all();

        $colors = array_map(function($el) {
            unset($el['translation']);
            return $el;
        }, $colors);

        return [
          'makes' => $makes,
          'colors' => $colors
        ];
    }
}