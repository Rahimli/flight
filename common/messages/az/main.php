<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/13/17
 * Time: 9:34 AM
 */

return [
    'Name' => 'Ad',
    'Subject' => 'Mövzu',
    'Message' => 'Mesaj',
    'Send' => 'Göndər',
    'Our contacts' => 'Bizimlə əlaqə',
    'Contacts' => 'Əlaqə',
    'Photo gallery' => 'Galereya',
    'Gallery' => 'Galereya',
    'News' => 'Xəbərlər',
    'Home' => 'Ana səhifə',
    'Company' => 'Şirkət',
    'Main office' => 'Baş ofis',
    'Working hours' => 'İş saatları',
    'Main office location' => 'Baş ofis ünvanı',
    'about us' => 'haqqımızda',
    'our plants' => 'Bitkilərimiz',
    'Fax' => 'Faks',
];