<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/13/17
 * Time: 9:51 AM
 */

namespace common\components;

use backend\models\Currency;
use common\models\Language;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Cookie;

class Controller extends \yii\rest\Controller
{
    public $languages;

    public function init()
    {
        parent::init();

        if(!Yii::$app->session->has('languages')) {
            $langs = Language::find()->select('code')->where(['enabled' => 1])->asArray()->all();
            foreach ($langs as $lang) {
                $_SESSION['languages'][] = $lang['code'];
            }
        }

        if (!empty($_GET['language']) && in_array($_GET['language'], $_SESSION['languages'])) {
            $_SESSION['language'] = $_GET['language'];
        }

        if (!Yii::$app->user->isGuest) {
            $lang = Language::find()->select('code')
                ->where(['id' => Yii::$app->user->identity->language_id])->asArray()->one();

            if ($lang) {
                $_SESSION['language'] = $lang['code'];
            }

        }

        if (empty($_SESSION['language'])) {
            $_SESSION['language'] = Yii::$app->params['defaultLanguage'];
        }

        //set lang
        Yii::$app->language = $_SESSION['language'];

    }

}