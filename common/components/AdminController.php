<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/13/17
 * Time: 9:51 AM
 */

namespace common\components;

use common\models\Language;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Cookie;

class AdminController extends \yii\web\Controller
{
    public $languages;

    public function init()
    {
        parent::init();

        if(!Yii::$app->session->has('languages')) {
            $langs = Language::find()->select('code')->where(['enabled' => 1])->asArray()->all();
            foreach ($langs as $lang) {
                $_SESSION['languages'][] = $lang['code'];
            }
        }

        if (empty($_SESSION['language'])) {
            $_SESSION['language'] = Yii::$app->params['defaultLanguage'];
        }
        //set lang
        Yii::$app->language = $_SESSION['language'];


    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

}