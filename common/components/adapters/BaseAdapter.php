<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 5/12/18
 * Time: 2:13 PM
 */

namespace common\components\adapters;


use GuzzleHttp\Client;

abstract class BaseAdapter
{
    protected $client;

    public function __construct(){
        $this->client = new Client();
    }

    protected abstract function getUrl(): string;
}