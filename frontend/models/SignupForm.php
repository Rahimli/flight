<?php
namespace frontend\models;

use common\models\Driver;
use common\models\UserVehicle;
use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const CHANNEL_WEB = 1;
    const CHANNEL_ANDROID = 2;
    const CHANNEL_IOS = 3;

    public $email;
    public $password;
    public $confirm;
    public $phone;
    public $agreement;
    public $country_id;
    public $type_id;
    public $make_id;
    public $model_id;
    public $car_number;
    public $first_name;
    public $last_name;
    public $channel_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'trim'],
            [['first_name', 'last_name', 'email', 'phone', 'agreement', 'type_id'], 'required'],

            [['first_name', 'last_name', 'phone'], 'string', 'min' => 3, 'max' => 32],
            [['first_name', 'last_name'], 'match', 'pattern' => '/^[a-zA-Z]*$/' ],

            [['password', 'confirm'], 'match', 'pattern' => '/^(?=.*[0-9])(?=.*[A-Z])([a-zA-Z0-9]+)$/',
                'message' => Yii::t('main','Şifrə minimum 1 böyük hərf, 1 rəqəm və hərflərdən ibarət olmalıdır')],

            ['agreement', 'boolean'],
            [['type_id', 'country_id'], 'integer'],

            [['channel_id'], 'integer'],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Bu email artıq mövcuddur'],

            ['confirm', 'compare', 'compareAttribute' => 'password'],
            [['password', 'confirm'], 'required'],
            [['password', 'confirm'], 'string', 'min' => 8, 'max' => 32],

            [
                ['first_name', 'last_name', 'email', 'phone', 'password', 'confirm'],
                'filter',
                'filter' => function ($value) {
                    return filter_var($value, FILTER_SANITIZE_STRING);
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('main', 'Email'),
            'first_name' => Yii::t('main', 'First name'),
            'last_name' => Yii::t('main', 'Last name'),
            'paasword' => Yii::t('main', 'Password'),
            'confirm' => Yii::t('main', 'Password (again)'),
            'phone' => Yii::t('main', 'Phone'),

        ];
    }


    /**
     * Signs user up.
     *
     * @return Driver|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new Driver();
        //set scenario
        $user->scenario = Driver::SCENARIO_MANUAL;
        $user->first_name = ucfirst($this->first_name);
        $user->last_name = ucfirst($this->last_name);
        $user->username = $user->first_name . ' ' . $user->last_name;
        $user->email = $this->email;
        $user->status = Driver::STATUS_DELETED;
        $user->phone = $this->phone;
        $user->ip_address = Yii::$app->request->getUserIP();
        $user->type_id = $this->type_id;
        $user->country_id = $this->country_id;
        $user->social = Driver::SIGNUP_MANUAL;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        //save user
        $user->save();

        if ($user->type_id == Driver::TYPE_DRIVER) {
            $user->createDriverRide();
            // after created driver ride
            $user->createDriverDeposit();
        }

        return $user;
    }

    /**
     * @param $user User
     * @return array signup success data
    */
    public function getSuccessData(User $user): array
    {
        $data = ['success' => 1];
        if ($this->channel_id == self::CHANNEL_WEB) {
            $data['message'] = Yii::t('main', 'Registration successfully completed. Check your email to confirm your account.');
        } else {
            $data['message'] = Yii::t('main', 'Registration successfully completed.');
            $data['userData'] = $user->getLoggedInData();
        }
        return $data;
    }

    public static function sendActivateEmail(User $user)
    {
        return Yii::$app->mailer->compose(['html' => 'site/signup'], ['model' => $user])
            ->setTo($user->email)
            ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
            ->setSubject(Yii::t('main', 'Activate your email on Shofero'))
            ->send();
    }

    public static function sendWelcomeEmail(User $user)
    {
        return Yii::$app->mailer->compose(['html' => 'site/welcome'], ['model' => $user])
            ->setTo($user->email)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setSubject(Yii::t('main', 'Welcome to Shofero!'))
            ->send();
    }
}
