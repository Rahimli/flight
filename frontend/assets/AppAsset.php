<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/reset.css',
        'css/landing.css',
        'css/fonts.css',
        'css/font-awesome.min.css',
        'css/ajax-loader.css',
        'css/media.css',
        'css/register.css',
    ];
    public $js = [
        'js/main.js',
        'js/input-mask.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
