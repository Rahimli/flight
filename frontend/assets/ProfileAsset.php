<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProfileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/reset.css',
        'css/fonts.css',
        'css/font-awesome.min.css',
        'css/ajax-loader.css',
        'css/profile.css',
        'css/media.css',
        'css/croppie.css',
    ];
    public $js = [
        'js/main.js',
        'js/input-mask.js',
        'js/profile.js',
        'js/croppie.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
