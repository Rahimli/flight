<?php
    use yii\widgets\Menu;
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!--     Custom CSS     -->
    <link rel="stylesheet" href="/css/style.css">
    <!-- CSS Files -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/material-kit.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/css/demo.css" rel="stylesheet" />
</head>

<body>
<div class="container-fluid">
    <br>
    <br>

    <!--Top menu-->

    <div class="row">
        <div class="col-md-2">
            <!-- LOGO   -->
        </div>
        <div class="col-md-10">

            <?= Menu::widget([
                'items' => [
                    ['label' => '<i class="material-icons my_icon">flight</i> Flight', 'url' => ['site/index']],
                    ['label' => '<i class="material-icons my_icon">domain</i> Hotels', 'url' => ['site/hotels']],
                    ['label' => '<i class="material-icons my_icon">flight domain</i> Flight & Hotels', 'url' => '#'],
                    ['label' => '<i class="material-icons my_icon">work</i>Tour Packages', 'url' => '#'],
                    ['label' => '<i class="material-icons my_icon">drive_eta</i> Rent car', 'url' => ['site/rent-car']],
                    ['label' => '<i class="material-icons my_icon">directions_boat</i> Ship', 'url' => '#'],
                    ['label' => '<i class="material-icons my_icon">home</i> Rent home', 'url' => '#'],
                    ['label' => '<i class="material-icons my_icon">credit_card</i> Visa', 'url' => 'http://vizam.az/'],
                ],
                'encodeLabels' => false,
                'options' => [
                    'class' => 'navbar-nav navnav nav-pills nav-pills-primary mother_menu',
                    'id'=>'site-navbar',
                    'role'=>'tablist',
                ],
                'linkTemplate' => '<a href="{url}" class="btn btn-default">{label}</a>'
            ]);
            ?>

        </div>
    </div>
    <br>
    <br>
    <?= $content?>

</div>
</body>

<!--   Font Awesome Icons-->
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<!--   Color hex sass-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--   Core JS Files   -->
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/material.min.js"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/js/nouislider.min.js" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="/js/bootstrap-datepicker.js" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="/js/material-kit.js" type="text/javascript"></script>

<script type="text/javascript">
    //    Radius-Slider
    $('#sliderRegular').noUiSlider({
        start: 40,
        connect: "lower",
        range: {
            min: 0,
            max: 100
        }
    });
</script>

</html>