<!--Services types-->
<div class="row">
    <div class="col-md-6 col-md-offset-1">
        <ul class="service">
            <li>
                <a href="#">
                    <button class="btn btn-primary">Roundtrip</button>
                </a>
            </li>
            <li>
                <a href="#">
                    <button class="btn btn-primary">One way</button>
                </a>
            </li>
            <li>
                <a href="#">
                    <button class="btn btn-primary">Multi city</button>
                </a>
            </li>
        </ul>
    </div>
</div>
<br>

<!--filtir types-->
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <div class="row">
                <section class="label_here">
                    <div class="col-md-2">
                        <label class="control-label my_label">Where from ?</label>
                        <input class="form-group filtr" type="text" name="where from" placeholder="anywhere">
                        <div id="sliderRegular" class="slider"></div>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label my_label">Where to ?</label>
                        <input class="form-group filtr" type="text" name="where from" placeholder="anywhere">
                        <div id="sliderRegular1" class="slider"></div>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label my_label">Departure from</label>
                        <input type="text" class="form-group filtr datetimepicker" placeholder="Departure date" />
                    </div>
                    <div class="col-md-2">
                        <label class="control-label my_label">Departure to</label>
                        <input type="text" class="form-group filtr datetimepicker" placeholder="Return date" />
                    </div>
                    <div class="col-md-2">
                        <label class="control-label my_label">Passengers count</label>
                        <input type="text" class="form-group filtr" name="Passengers_count">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label my_label">Pick your cabin class</label>
                        <select class="form-group filtr">
                            <option value="first_class" class="form-group">First Class</option>
                            <option value="business">Business</option>
                            <option value="premium_economy">Premium economy</option>
                            <option value="economy_coach">Economy / coach</option>
                        </select>
                    </div>
                </section>
            </div>
        </div>
        <!-- search btn -->
        <div class="col-md-1">
            <button class="btn btn-primary"><i class="material-icons">&#xE8B6;</i>Search</button>
        </div>
    </div>
</div>

<!--   Modal filter begin here  -->
<!--   Model where from  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="row">
            <div class="col-md-8">
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title text" id="myModalLabel">Please, pick your destination</h2>
                    </div>
                    <div class="modal-body">
                        <ul class="modal_content text-center">
                            <li>
                                <input type="text" class="form-control" placeholder="Country, city, airport or from anywhere">
                            </li>
                            <br>
                            <li>
                                <a href="#">
                                    <div id="sliderRegular" class="slider"></div>
                                </a>
                            </li>
                            <li>
                                <input type="text" class="form-control" placeholder="Space">
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info btn-success">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Modal where to  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title text" id="myModalLabel">Please, pick your destination</h2>
            </div>
            <div class="modal-body">
                <ol class="modal_content">
                    <li>
                        <input type="text" class="form-control" placeholder="Country, city, airport or to anywhere">
                    </li>
                    <li>
                        <a href="#">
                            <h4>Space</h4>
                        </a>
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info btn-simple">OK</button>
            </div>
        </div>
    </div>
</div>
<!--Footer Section-->