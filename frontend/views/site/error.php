<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = Yii::t('main', $name);
?>
<div class="container mar-t-50">
    <div class="content">
        <section  class="careers">
            <div class="text-center">
                <h1 class="t1"><?= $this->title?></h1>
                <hr class="t1"/>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <?= Html::encode($this->title) ?>
                    </div>

                    <p>
                        <?=Yii::t('yii','The above error occurred while the Web server was processing your request.')?>
                    </p>
                    <p>
                        <?=Yii::t('yii','Please contact us if you think this is a server error. Thank you.')?>
                    </p>
                </div>
            </div>
        </section>
    </div>
</div>


