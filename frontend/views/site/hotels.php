<!--filtir types-->
<div class="container">
    <!-- Going To -->
    <div class="row justify-content-md-center">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <form role="form-group" action="#">
                <div class="form-group">
                    <input type="text" class="form-control dest_input filtr empty" id="iconified"
                           placeholder=" &#xE0C8; destination, hotel name, airport, train station, landmark, adress"/>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="row">
                <section class="label_here">
                    <ul class="service">
                        <li><input type="text" class="form-group filtr datetimepicker" placeholder="Chek in"/></li>
                        <li><input type="text" id="formGroupExampleInput" class="form-group filtr datetimepicker"
                                   placeholder="Chek out"/></li>
                        <li><input class="form-group filtr" data-toggle="modal" data-target="#myModal"
                                   placeholder="Travelers">
                        </li>
                    </ul>
                </section>
            </div>
        </div>

    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="optionsCheckboxes">
                    <i class="material-icons my_icon">flight</i><span class="add_trans">Add a flight</span>
                </label>

            </div>
        </div>
        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="optionsCheckboxes">
                    <i class="material-icons my_icon">drive_eta</i><span class="add_trans">Add a car</span>
                </label>

            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 offset-col-md-1">
            <form role="form-group" action="#">
                <div class="form-group">
                    <input type="text" class="form-control filtr empty backg" id="iconified"
                           placeholder=" &#xE0C8; City or airport" style="background: #fff !important;"/>
                </div>
            </form>
        </div>
    </div>
    <!-- search btn -->
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-1">
            <button class="btn btn-primary"><!-- <i class="material-icons">&#xE8B6;</i> -->Search Hotel + Flight +
                Car
            </button>
        </div>
    </div>

</div>

<!-- Modal travelers -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close x" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text" id="myModalLabel">Room 1</h4>
                <hr>
            </div>
            <div class="modal-body">
                <ul class="modal_content">
                    <li>
                        <label>Adult</label> <input type="number" class="form-group filtr"
                                                    name="adult-count">
                    </li>
                    <li>
                        <label>Children</label> <input type="number" class="form-group filtr"
                                                       name="children-counts">
                    </li>
                    <li><label>Child age</label>
                        <select class="form-group filtr">
                            <option value="Age">Age</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="another-room">
                <ul class="modal_content">
                    <li><span><i class="material-icons">control_point</i>Add another room</span></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info btn-success">OK</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#iconified').on('keyup', function () {
    var input = $(this);
    if (input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
</script>