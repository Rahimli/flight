<!--filtir types-->
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <form action="#" method="">
                    <input type="text" class="form-control filtr empty backg" id="iconified" placeholder=" &#xE0C8; City or airport" style="background: #fff !important;" />
                </form>
            </div>
            <div id="sliderRegular" class="slider"></div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <form action="#" method="">
                    <input type="text" class="form-control filtr empty backg" id="iconified" placeholder=" &#xE0C8; City or airport" style="background: #fff !important;" />
                </form>
            </div>
            <div id="sliderRegular" class="slider"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <form action="#" method="">
                <input type="text" class="form-group filtr datetimepicker" style="width: 100%;color: #ffffff !important" placeholder="Pick-up date" />
            </form>
        </div>
        <div class="col-md-1">
            <form action="#" method="">
                <select class="form-group filtr" style="width: 100%;color: #ffffff !important">
                    <option value="22:26">22:26</option>
                    <option value="22:26">22:26</option>
                    <option value="22:26">22:26</option>
                    <option value="22:26">22:26</option>
                </select>
            </form>
        </div>
        <div class="col-md-3">
            <form action="#" method="">
                <input type="text" class="form-group filtr datetimepicker" style="width: 100%;color: #ffffff !important" placeholder="Drop-off date" />
            </form>
        </div>
        <div class="col-md-1">
            <form action="#" method="">
                <select class="form-group filtr" style="width: 100%;color: #ffffff !important">
                    <option value="14:00">14:00</option>
                    <option value="14:00">14:00</option>
                    <option value="14:00">14:00</option>
                    <option value="14:00">14:00</option>
                </select>
            </form>
        </div>
    </div>
    <!-- advenced options -->
    <div class="row">
        <div class="col-md-12">
            <div class="advenced-foot">
                <a href="#">Advenced options</a>
            </div>
        </div>
        <ul class="advenced-preferce">
            <li>
                <select style="width: 50%">
                    <option>No Preferce</option>
                    <option>No Preferce</option>
                </select>
            </li>
            <li>
                <select style="width: 50%">
                    <option>No Preferce</option>
                    <option>No Preferce</option>
                </select>
            </li>
            <li>
                <select style="width: 50%">
                    <option>I don't have a code</option>
                    <option>I don't have a code</option>
                </select>
            </li>
        </ul>
    </div>
    <div class="row">
        <span style="color: #ddd;">Special equipment</span>
        <form action="#" method="">
            <div class="col-md-3">
                <ul class="drive-foot">
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Infant seat</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Toader seat</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Navigation system</span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="drive-foot">
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Ski rack</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Snow chains</span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="drive-foot">
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">Left hand control</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="optionsCheckboxes">
                                <span class="add_trans">right hand control</span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-2">
            <button class="btn btn-primary"><i class="material-icons">&#xE8B6;</i>Search</button>
        </div>
    </div>
</div>