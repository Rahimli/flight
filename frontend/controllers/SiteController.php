<?php
namespace frontend\controllers;

use backend\models\Page;
use common\components\AuthHandler;
use common\components\order\OrderRequest;
use common\components\order\DistanceCalculator;
use common\models\LoginForm;
use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use common\components\WebController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;

/**
 * Site controller
 */
class SiteController extends WebController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'signup-continue', 'driver-license', 'complete-signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'signup-continue', 'driver-license', 'complete-signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    //authclient authentication
    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    public function actionDistance()
    {
        $origin = ['latitude' => 40.4141781, 'longitude' => 49.8861694];
        $destination = ['latitude' => 40.4280308, 'longitude' => 49.909687];

        echo DistanceCalculator::calculatePrice($origin, $destination, 5);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Hotels page
     *
     * @return mixed
     */
    public function actionHotels()
    {
        return $this->render('hotels');
    }

    /**
     * Hotels page
     *
     * @return mixed
     */
    public function actionRentCar()
    {
        return $this->render('rent-car');
    }

    /**
     * Signup continue
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'register';
        $model = new LoginForm();
        return $this->render('login',[
            'model' => $model
        ]);
    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        if (Yii::$app->request->isAjax) {

            \Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
            Yii::$app->user->logout();
            return [
                'success' => 1,
                'returnUrl' => Url::to(['/site/login'])
            ];
        }
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        \Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('main','Check your email for further instructions'));

                return [
                    'success' => 1,
                    'message' => Yii::t('main', 'Check your email for further instructions')
                ];
            } else {
                return [
                    'success' => 0,
                    'message' => Yii::t('main', 'Sorry, we are unable to reset password for the provided email address')
                ];
            }
        } else {
            return [
                'success' => 0,
                'message' => array_values($model->getFirstErrors())[0]
            ];
        }

    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $message = Yii::t('main', 'New password saved.').
                ' <a href="#" class="open-signin">'.Yii::t('main', 'Log in').'</a>';
            Yii::$app->session->setFlash('alert_message_rp', $message);

        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionConfirmRegistration($key)
    {
        $user = User::find()->where(['auth_key'=>$key])->one();

        if(!$user) {
            return $this->redirect(Yii::$app->urlManager->createUrl(['/']));
        }
        if($user->status == User::STATUS_ACTIVE){
            Yii::$app->session->setFlash('alert_message',
                Yii::t('main', 'Account already confirmed'));
        }

        if ($user->status == User::STATUS_DELETED) {
            Yii::$app->session->setFlash('alert_message',
                Yii::t('main', 'Account successfully confirmed'));

            Yii::$app->session->setFlash('alert_email',$user->email);
            $user->status = User::STATUS_ACTIVE;

            $user->save();

            //SignupForm::sendWelcomeEmail($user); TODO

        }
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/site/signup-continue']);
        } else {
            return $this->redirect(['/site/login']);
        }
    }

//    public function actionTestActivateEmail()
//    {
//        $user = User::findOne(23);
//        SignupForm::sendActivateEmail($user);
//    }

    public function actionSetLang() {
        $langCodes = $_SESSION['languages'];
        $language = Yii::$app->request->get('language', false);
        $url = Yii::$app->request->referrer;
        if($language && in_array($language, $langCodes)) {
            if($_SESSION['route'] == 'media/index') {
                $pageid = Page::find()->where(['slug' => $_SESSION['slug']])->joinWith('translation')->one();
                Yii::$app->language = $language;
                $media = Page::find()->where(['page.id' => $pageid->id])->localized(Yii::$app->language)->joinWith('translation')->one();
                $url = Url::to([$_SESSION['route'], 'slug'=>$media->slug]);
                $_SESSION['language'] = $language;
                return $this->redirect($url);
            }
            elseif ($_SESSION['route'] == 'media/view'){
                $mediaid = Media::find()->where(['slug' => $_SESSION['slug']])->joinWith('translation')->one();
                Yii::$app->language = $language;
                $media = Media::find()->where(['media.id' => $mediaid->id])->localized(Yii::$app->language)->joinWith('translation')->one();
                $url = Url::to([$_SESSION['route'], 'slug1'=>$media->page->slug, 'slug' => $media->slug]);
                $_SESSION['language'] = $language;
                return $this->redirect($url);
            }

            Yii::$app->language = $language;
            $_SESSION['language'] = $language;

            $urlArray = explode('/', $url);

            if(count($urlArray) > 3) {
                if(!isset($urlArray[4])) {
                    $urlArray[4] = '';
                }

                $urlArray[3] = $language;
                $url = implode('/', $urlArray);
                return $this->redirect($url);
            }
        }

        return $this->redirect($url);
    }
}
