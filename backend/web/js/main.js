$(document).ready(function(){

    $(document).on('click', '#information-has_editor', function() {
        var url = document.location.href.split("?")[0];
        if($(this).is(':checked')) {
            url += "?editor=1";
        } else {
            url += "?editor=0";
        }
        document.location = url;
    });

    $(document).on('click', '.menu-sequence', function(e) {
        e.preventDefault();
        var _this = $(this);
        $.ajax({
            url: '/admin/page/change-sequence',
            dataType: 'json',
            type: 'post',
            data: {id:_this.data('id'), type: _this.data('type')},
            success: function (data) {
                if (data.success === 1) {
                    location.reload();
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $(document).on('click', '.product-activate', function(e) {
        e.preventDefault();
        var _this = $(this);
        $(_this).attr("disabled", true);
        $.ajax({
            url: '/admin/product/change-status',
            dataType: 'json',
            type: 'post',
            data: {id:_this.data('id'), type: _this.data('type')},
            success: function (data) {
                if (data.success === 1) {
                    location.reload();
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $(document).on('click', '.logout', function(e) {
        e.preventDefault();
        $.ajax({
            url: '/admin/site/logout',
            dataType: 'json',
            type: 'post',
            success: function (data) {
                if (data.success === 1) {
                    location.href = data.returnUrl;
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    })

    //option change change option value view (option value)
    $(document).on('change', '#optionvalue-option_id', function (e) {
        var optionSelected = $("option:selected", this);
        if (optionSelected.data('translated') == 1) {
            $('.option-value-form .nav-tabs').fadeIn(50);
            $('.option-value-form .tab-content').fadeIn(50);
            $('.option-value-form .field-optionvalue-int_value').fadeOut(50);
        } else {
            $('.option-value-form .nav-tabs').fadeOut(50);
            $('.option-value-form .tab-content').fadeOut(50);
            $('.option-value-form .field-optionvalue-int_value').fadeIn(50);
        }
    });

});