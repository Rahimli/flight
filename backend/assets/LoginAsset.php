<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admintheme/css/bootstrap.min.css',
        'admintheme/css/light-bootstrap-dashboard.css?v=1.4.0',

    ];
    public $js = [

    ];
    public $depends = ['yii\web\JqueryAsset','yii\bootstrap\BootstrapAsset'];

}
