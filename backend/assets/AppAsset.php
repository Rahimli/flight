<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admintheme/css/bootstrap.min.css',
        'css/site.css',
        'datepicker/css/bootstrap-datepicker3.min.css',
        'admintheme/css/animate.min.css',
        'admintheme/css/light-bootstrap-dashboard.css?v=1.4.0',
        'admintheme/css/demo.css',
        'admintheme/css/pe-icon-7-stroke.css'
    ];
    public $js = [
        'admintheme/js/bootstrap.min.js',
        'js/slugify.js',
        'js/main.js',
        'datepicker/js/bootstrap-datepicker.min.js',
        'admintheme/js/chartist.min.js',
        'admintheme/js/bootstrap-notify.js',
        'admintheme/js/light-bootstrap-dashboard.js?v=1.4.0',
        'admintheme/js/demo.js'
    ];
    public $depends = ['yii\web\JqueryAsset',];
    public function init()
    {
        parent::init();
        Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
    }
}
