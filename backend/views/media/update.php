<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = Yii::t('main', 'Update {modelClass}: ', [
    'modelClass' => 'Media',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="media-update">
    <div class="card">
        <div class="header">
            <h4 class="title"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="content">

    <?= $this->render('_form', [
        'model' => $model,
        'pages' => $pages
    ]) ?>
        </div>
    </div>
</div>
