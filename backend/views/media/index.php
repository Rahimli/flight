<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">
    <div class="card">
        <div class="header">
            <h4 class="title"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="content">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Media'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
<?php Pjax::begin(); ?>
<?php
    $this->registerJs(' 
       $("input[name*=\'created_at\']").datepicker({
            format: \'yyyy-mm-dd\',
            autoclose: true
        });
        $("input[name*=\'[updated_at]\']").datepicker({
            format: \'yyyy-mm-dd\',
            autoclose: true

        });'); ?>
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'created_at',
                'value' => function($data) {
                    return date('Y-m-d H:i:s', $data->created_at);
                },

            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data) {
                    return date('Y-m-d H:i:s', $data->updated_at);
                }
            ],
            'view_count',
            // 'enabled',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
    </div>
</div>
