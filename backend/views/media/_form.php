<?php

use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Media */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs("CKEDITOR.plugins.addExternal('justify', '/js/ckeditor/justify/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('confighelper', '/js/ckeditor/confighelper/plugin.js', '');");
?>

<div class="media-form">
    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>"><a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a></li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(['id' => 'media-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <?= $form->field($model, 'title'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => true,'data-slugify'=>"#media-slug".(Yii::$app->language == $lang?'':'_'.$lang)]) ?>
                <?= $form->field($model, 'slug'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'short_desc'.(Yii::$app->language == $lang?'':'_'.$lang))->widget(CKEditor::className(), [
                    'options' => ['rows' => 4],
                    'preset' => 'full',
                    'clientOptions' => [
                        'extraPlugins' => 'justify,confighelper',
                        'height' => '150px',
                        'allowedContent' => false,
                        'filebrowserUploadUrl' => '/admin/site/upload-image',
                        'placeholder' => 'Short description...'
                    ]
                ])->label(false) ?>

                <?= $form->field($model, 'description'.(Yii::$app->language == $lang?'':'_'.$lang))->widget(CKEditor::className(), [
                    'options' => ['rows' => 4],
                    'preset' => 'full',
                    'clientOptions' => [
                        'extraPlugins' => 'justify,confighelper',
                        'height' => '300px',
                        'allowedContent' => false,
                        'filebrowserUploadUrl' => '/admin/site/upload-image',
                        'placeholder' => 'Description...'
                    ]
                ])->label(false) ?>
            </div>

        <?php endforeach;?>
    </div>
    <?= FileInput::widget([
        'name' => 'Media[image]',
        'pluginOptions' => [
            'initialPreview' => $model->image ? Yii::getAlias('@media/').$model->image : false,
            'initialPreviewAsData' => !empty($model->image),
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  $model->isNewRecord ? 'Select image' : 'Change media image'
        ],
        'options' => ['accept' => 'image/*']
    ]);?>

    <?= $form->field($model, 'page_id')->dropDownList($pages, ['prompt' => 'Select page']) ?>


    <?= $form->field($model, 'enabled')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
