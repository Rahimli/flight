<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Color */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="color-form">

    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>"><a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a></li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <?= $form->field($model, 'name'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => 50]) ?>
            </div>
        <?php endforeach;?>
    </div>

    <?= $form->field($model, 'value')->input('color', ['maxlength' => true, 'style' => 'width: 100px;']) ?>

    <?= $form->field($model, 'enabled')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
