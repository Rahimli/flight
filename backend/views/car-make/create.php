<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CarMake */

$this->title = Yii::t('main', 'Create Car Make');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Car Makes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-make-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
