<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user->username;
                }
            ],
            [
                'attribute' => 'driver_id',
                'value' => function($model) {
                    return $model->driver->username;
                }
            ],
            'from_location',
            'to_location',
            'price',
            [
                'attribute' => 'started_at',
                'value' => function($data) {
                    return $data->started_at ? date('d M, Y H:i', $data->started_at) : null;
                }
            ],
            [
                'attribute' => 'ended_at',
                'value' => function($data) {
                    return $data->ended_at ? date('d M, Y H:i', $data->ended_at): null;
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($data) {
                    return $data->created_at ? date('d M, Y H:i', $data->created_at): null;
                }
            ],
            [
                'attribute' => 'status_id',
                'value' => function($model) {
                    return $model->getStatusName();
                },
                'filter'=>\common\components\order\OrderUtils::getStatuses()
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
