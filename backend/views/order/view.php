<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = Yii::t('main', 'Order') . ' #' .$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$notes = $model->driverMessages;
$userReview = $model->userReview;
$driverReview = $model->driverReview;

?>
<div class="order-view">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <?php if ($userReview):?>
            <div class="<?= $driverReview ? 'col-md-6': 'col-md-12'?> order-review">
                <div class="card card-user pad-20">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="avatar border-gray" src="<?= $model->user->getProfileImg()?>">
                        </div>
                        <div class="col-md-9">
                            <h4><?= $model->user->username?></h4>
                            <p class="rating">
                                <?php foreach (range(1, $userReview->rating) as $i):?>
                                    <i class="fa fa-star"></i>
                                <?php endforeach;?>
                                <?php if ($userReview->rating < 5):?>
                                    <?php foreach (range(0, 4-$userReview->rating) as $i):?>
                                        <i class="fa fa-star-o"></i>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </p>
                            <p class="user-type">
                                <?=$userReview->getTypeLabel()?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?=$userReview->review?>
                        </div>
                    </div>

                </div>
            </div>
        <?php endif;?>
        <?php if ($driverReview):?>
            <div class="<?= $userReview ? 'col-md-6': 'col-md-12'?> order-review">
                <div class="card card-user pad-20">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="avatar border-gray" src="<?= $model->driver->getProfileImg()?>">
                        </div>
                        <div class="col-md-9">
                            <h4><?= $model->driver->username?></h4>
                            <p class="rating">
                                <?php foreach (range(1, $driverReview->rating) as $i):?>
                                    <i class="fa fa-star yellow"></i>
                                <?php endforeach;?>
                                <?php if ($driverReview->rating < 5):?>
                                    <?php foreach (range(0, 4-$driverReview->rating) as $i):?>
                                        <i class="fa fa-star-o"></i>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </p>
                            <p class="user-type">
                                <?=$driverReview->getTypeLabel()?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?=$driverReview->review?>
                        </div>
                    </div>

                </div>
            </div>
        <?php endif;?>
        <?php if ($notes):?>
            <div class="col-md-5">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Notes</h4>
                        <p class="category">Notes which user has sent to driver</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                            <th>ID</th>
                            <th><?= Yii::t('main', 'Message')?></th>
                            </thead>
                            <tbody>
                            <?php foreach ($notes as $note):?>
                                <tr>
                                    <td><?= $note->id?></td>
                                    <td><?= $note->message?></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if ($model->orderStopPoints):?>
            <div class="<?= $notes ? 'col-md-7': 'col-md-12'?>">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Order stop points</h4>
                        <p class="category">Driver stopped for each point below</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th>ID</th>
                                <th><?= Yii::t('main', 'Location')?></th>
                                <th><?= Yii::t('main', 'Latitude')?></th>
                                <th><?= Yii::t('main', 'Longitude')?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($model->orderStopPoints as $point):?>
                                    <tr>
                                        <td><?= $point->id?></td>
                                        <td><?= $point->location?></td>
                                        <td><?= $point->latitude?></td>
                                        <td><?= $point->longitude?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        <?php endif;?>

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Order</h4>
                    <p class="category">Advanced order details</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'user_id',
                                'value' => function($model) {
                                    return $model->user->username;
                                }
                            ],
                            [
                                'attribute' => 'driver_id',
                                'value' => function($model) {
                                    return $model->driver->username;
                                }
                            ],
                            'from_location',
                            'to_location',
                            'distance',
                            'price',
                            [
                                'attribute' => 'started_at',
                                'value' => function($data) {
                                    return $data->started_at ? date('d M, Y H:i', $data->started_at) : null;
                                }
                            ],
                            [
                                'attribute' => 'ended_at',
                                'value' => function($data) {
                                    return $data->ended_at ? date('d M, Y H:i', $data->ended_at): null;
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function($data) {
                                    return $data->created_at ? date('d M, Y H:i', $data->created_at): null;
                                }
                            ],
                            [
                                'attribute' => 'status_id',
                                'value' => function($model) {
                                    return $model->getStatusName();
                                }
                            ]
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
