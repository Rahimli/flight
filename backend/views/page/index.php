<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <div class="card">
        <div class="header">
            <h4 class="title"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="content">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create page'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'slug',
            [
                'attribute' => 'parent_id',
                'value' => function($data) { return $data->parent_id ? $data->parent->title: null;}
            ],
            [
                'attribute' => 'image',
                'value' => function($data) { return Yii::getAlias('@page/').$data->image; },
                'format' => ['image',['width'=>'100','height'=>'50']],
            ],
            'enabled',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {upButton} {downButton}',
                'buttons' => [
                    'upButton' => function($url, $model, $key) {
                        $html = "<a href='#' class='menu-sequence' data-id='$model->id' data-type='up'><span class='glyphicon glyphicon-arrow-up' aria-hidden='true'></span> </a>";
                        return $html;
                    },
                    'downButton' => function($url, $model, $key) {
                        $html = "<a href='#' class='menu-sequence' data-id='$model->id' data-type='down'><span class='glyphicon glyphicon-arrow-down' aria-hidden='true'></span></a>";
                        return $html;
                    }
        ]
        ]
        ],
    ]); ?>
<?php Pjax::end(); ?>
        </div>
    </div>
</div>
