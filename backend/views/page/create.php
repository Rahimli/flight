<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = Yii::t('main', 'Create Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">

    <div class="card">
        <div class="header">
            <h4 class="title"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="content">

    <?= $this->render('_form', [
        'model' => $model,
        'pages' => $pages,
    ]) ?>
        </div>
    </div>
</div>
