<?php

use dosamigos\ckeditor\CKEditor;
use dosamigos\fileupload\FileUploadUI;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("CKEDITOR.plugins.addExternal('justify', '/admin/js/ckeditor/justify/plugin.js', '');");
?>

<div class="Page-form">
    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>"><a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a></li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <?= $form->field($model, 'title'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => true, 'data-slugify'=>"#page-slug".(Yii::$app->language == $lang?'':'_'.$lang)]) ?>
                <input type="hidden" name="Page[language_<?= $lang?>]" value="<?=$lang?>"/>
                <?= $form->field($model, 'slug'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text'.(Yii::$app->language == $lang?'':'_'.$lang))->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'full',
                    'clientOptions' => [
                        'extraPlugins' => 'justify',
                        'filebrowserUploadUrl' => Yii::getAlias('@web').'/site/upload-image'
                    ]
                ]) ?>
            </div>
        <?php endforeach;?>
    </div>

    <?php
    echo '<label class="control-label">Add Attachments</label>';
    if ($model->isNewRecord) {
        echo FileInput::widget([
            'model' => $model,
            'attribute' => 'image',
            'options' => ['multiple' => false],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/page/upload', 'id'=>$model->id]),

            ],
        ]);
    } else {
        echo FileInput::widget([
            'model' => $model,
            'attribute' => 'image',
            'options' => ['multiple' => false],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/page/upload', 'id'=>$model->id]),
                'initialPreview'=>[
                    Yii::getAlias('@page/').$model->image,
                ],
                'initialPreviewAsData'=>true,
                'initialCaption'=> $model->title,
                'initialPreviewConfig' => [
                    ['caption' => $model->title],
                ],
                'overwriteInitial'=>false,
            ],
        ]);
    }

    ?>

    <?= $form->field($model, 'parent_id')->dropDownList($pages, ['prompt'=>'Choose Parent']); ?>

    <?= $form->field($model, 'view_type')->dropDownList([
        'single' => 'Single page',
        'grid' => 'Grid view',
        'faq' => 'FAQ']);  ?>

    <?= $form->field($model, 'route')->textInput(['value' => $model->route ? $model->route : '/media/index']);  ?>


    <?= $form->field($model, 'enabled')->checkBox();  ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
