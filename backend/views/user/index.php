<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img src="' . $data->getProfileImg().'" style="max-width: 80px; max-height: 80px;" />';
                }
            ],
            'username',
            'email:email',
            'phone',
            [
                'attribute' => 'type_id',
                'value' => function($data) {
                    return $data->type_id ? \common\models\UserUtils::getTypeById($data->type_id) : null;
                }
            ],
            [
                'attribute' => 'role',
                'value' => function($data) {
                    return \common\models\UserUtils::getRoleByName($data->role);
                }
            ],
            'ip_address',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\models\UserUtils::getStatusById($data->status);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Yii::$app->user->can('manageUsers') ? '{view}{update}{delete}' : '{view}',
            ],
        ],
    ]); ?>
</div>
