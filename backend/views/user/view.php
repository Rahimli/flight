<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $ride common\models\DriverRide */

$this->title = Yii::t('main', 'User'). ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('manageUsers')):?>
        <p>
            <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif;?>
    <br/>
    <?php if (!empty($ride)):?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Driver Rides</h4>
                        <p class="category">Driver ride statistics based on rides</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                            <th><?= Yii::t('main', 'All rides count')?></th>
                            <th><?= Yii::t('main', 'Completed count')?></th>
                            <th><?= Yii::t('main', 'Cancelled count')?></th>
                            <th><?= Yii::t('main', 'Online hours')?></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $ride->accept_count?></td>
                                    <td><?= $ride->completed_count?></td>
                                    <td><?= $ride->cancel_count?></td>
                                    <td><?= $ride->getOnlineHours()?></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img src="'.$data->getProfileImg().'" style="max-width: 250px; max-height: 200px;" />';
                }
            ],
            'first_name',
            'last_name',
            'username',
            'email:email',
            'address',
            'phone',
            [
                'attribute' => 'country_id',
                'value' => function($data) {
                    return !empty($data->country) ? $data->country->name: null;
                }
            ],
            [
                'attribute' => 'language_id',
                'value' => function($data) {
                    return !empty($data->language) ? $data->language->language: null;
                }
            ],
            'login_count',
            [
                'attribute' => 'type_id',
                'value' => function($data) {
                    return \common\models\UserUtils::getTypeById($data->type_id);
                }
            ],
            [
                'attribute' => 'social',
                'value' => function($data) {
                    return \common\models\UserUtils::getsignupMethodById($data->social);
                }
            ],
            'ip_address',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\models\UserUtils::getStatusById($data->status);
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($data) {
                    return date('d M, Y H:i', $data->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data) {
                    return date('d M, Y H:i', $data->updated_at);
                }
            ],
        ],
    ]) ?>

</div>
