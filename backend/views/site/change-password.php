<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-change-pass">
    <div class="row">
        <div class="col-lg-4 col-md-offset-4 col-sm-offset-0">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'oldPassword')->passwordInput();?>

            <?= $form->field($model, 'newPassword')->passwordInput();?>

            <?= $form->field($model, 'confirmPassword')->passwordInput();?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('main', 'Change password'), ['class' => 'btn btn-primary']) ?>
            </div>
            <?php $form = ActiveForm::end(); ?>
        </div>
    </div>
</div>
