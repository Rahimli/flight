<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DriverDocument */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Driver Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-document-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'driver_id',
                'value' => function($data) {
                    return $data->driver->username;
                }
            ],
            [
                'attribute' => 'driving_license',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->driving_license.'" />';
                }
            ],
            [
                'attribute' => 'conviction_certificate',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->conviction_certificate.'" />';
                }
            ],
            [
                'attribute' => 'id_card',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->id_card.'" />';
                }
            ],
        ],
    ]) ?>

</div>
