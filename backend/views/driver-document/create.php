<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DriverDocument */

$this->title = Yii::t('main', 'Create Driver Document');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Driver Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
