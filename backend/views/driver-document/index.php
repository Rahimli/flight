<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DriverDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Driver Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'username',
                'value' => function($data) {
                    return $data->driver->username;
                }
            ],
            [
                'attribute' => 'driving_license',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->driving_license.'" />';
                }
            ],
            [
                'attribute' => 'conviction_certificate',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->conviction_certificate.'" />';
                }
            ],
            [
                'attribute' => 'id_card',
                'format' => 'raw',
                'value' => function($data) {
                    return '<img class="grid-img" src="'.Yii::getAlias('@profile/'). $data->id_card.'" />';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
