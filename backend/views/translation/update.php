<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $models array */

$this->title = 'Update Translation: ' . $models['en']['translation'];
$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $models['en']['translation'], 'url' => ['view', 'id' => $models['en']['id'], 'language' => $models['en']['language']]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="translation-update">

        <div class="card">
            <div class="header">
                <h4 class="title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="content">

    <?= $this->render('_form', [
        'models' => $models,
        'isNewRecord' => false
    ]) ?>
            </div>
    </div>

</div>
