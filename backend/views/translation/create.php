<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Translation */

$this->title = 'Create Translation';
$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-create">

        <div class="card">
            <div class="header">
                <h4 class="title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="content">

    <?= $this->render('_form', [
        'model' => $model,
        'isNewRecord' => true
    ]) ?>
            </div>
    </div>

</div>
