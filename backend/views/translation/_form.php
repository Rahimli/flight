<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $models array */
/* @var $isNewRecord bool */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translation-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group field-translation-category">
        <label class="control-label" for="translation-category">Category</label>
        <input id="translation-category" value="<?= isset($models['category']) ? $models['category'] : ''?>" class="form-control" name="Translation[category]"/>
        <div class="help-block"></div>
    </div>
    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>"><a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a></li>
        <?php endforeach;?>
    </ul>

    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <br/>
                <div class="form-group field-translation-translation">
                    <label class="control-label" for="translation-translation">Translation <?=$lang?></label>
                    <textarea id="translation-translation" class="form-control" name="Translation[translation][<?=$lang?>]" rows="6" aria-invalid="false"><?= isset($models[$lang]) ? $models[$lang]['translation'] : ''?></textarea>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endforeach;?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($isNewRecord ? 'Create' : 'Update', ['class' => $isNewRecord ? 'btn btn-info btn-fill' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
