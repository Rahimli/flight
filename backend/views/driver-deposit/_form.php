<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DriverDeposit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-deposit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'driver_id')->dropDownList(
        $drivers,
        ['prompt'=>Yii::t('main', 'Select driver')]
    )->label(false); ?>

    <?= $form->field($model, 'deposit_count')->input('number') ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
