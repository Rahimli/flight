<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DriverDeposit */

$this->title = Yii::t('main', 'Create Driver Deposit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Driver Deposits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-deposit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'drivers' => $drivers
    ]) ?>

</div>
